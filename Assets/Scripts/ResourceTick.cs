﻿using System.Collections.Generic;
using UnityEngine;

public class ResourceTick : MonoBehaviour
{
    private float m_resource = 100.0f;
    private const float MAX_RESOURCE_AMOUNT = 100.0f;

    public string resourceName = "Resource";


    [Range(0.05F,10F)]
    public float factor = 0.05F;

    public bool inverted = false;

    private Transform m_indicator;


    private static Dictionary<string, ResourceTick> m_resources = new Dictionary<string, ResourceTick>(2);

    private void Awake()
    {
        //register at the local registry
        m_resources.Add(resourceName,this);
    }

    public static ResourceTick getResource(string name)
    {
        //return entry in registry
        return m_resources[name];
    }


    void Start()
    {
        m_indicator = transform.GetChild(0);

        //register at current player
        StaticGameManager.PlayerContainer.MurderCallbackActions.Add(OutOfResources);
    }

    /**@brief add some amount to this resource
     * @param [in] resourceAmount how much you want to add
     * */
    public void addResource(float resourceAmount = 10.0f)
    {
        if (MAX_RESOURCE_AMOUNT + resourceAmount > MAX_RESOURCE_AMOUNT) m_resource = MAX_RESOURCE_AMOUNT;
        else m_resource += resourceAmount;
    }

    /**@brief checks if the Resource is bare
     * @returns (bool: if the resource is depleted,string: the name of the resource)
     */
    public (bool,string) OutOfResources()
    {
        return  (m_resource <= 0, resourceName);
    }

    void Update()
    {

        //tick away from the resource by a given factor
        m_resource -= Time.deltaTime * factor;
        if (m_resource < 0) m_resource = 0;

        //indicated this by displaying a bar graph (can be inverted)
        m_indicator.localScale = new Vector3(inverted ? 1 - (m_resource / MAX_RESOURCE_AMOUNT) : (m_resource / MAX_RESOURCE_AMOUNT), 1, 1);

    }
}
