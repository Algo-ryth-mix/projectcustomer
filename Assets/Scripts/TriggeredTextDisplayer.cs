﻿using System;
using UnityEngine;
using UnityEngine.UI;

[Obsolete("has no real use, trigger-system use is questionable in itself")]
public class TriggeredTextDisplayer : MonoBehaviour
{
    [SerializeField] private ItemTrigger trigger;
    private Text m_text;
    
    [SerializeField]
    private string message = "Item Trigger condition was met.";

    void Start()
    {
        m_text = GetComponent<Text>();
    }

    private bool m_once = false;

    void Update()
    {
        if (trigger.WasTriggered() && !m_once)
        {
            m_once = true;
            m_text.text = message;
        }
    }
}
