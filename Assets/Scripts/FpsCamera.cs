﻿using UnityEngine;
using System.Collections.Generic;

[AddComponentMenu("Camera-Control/FPS Camera")]
public class FpsCamera : MonoBehaviour
{

    public enum RotationAxes
    {
        MouseXAndY = 0,
        MouseX = 1,
        MouseY = 2
    }

    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;
    public float minimumX = -360F;
    public float maximumX = 360F;
    public float minimumY = -60F;
    public float maximumY = 60F;
    public float frameCounter = 20;


    float m_rotationX = 0F;
    float m_rotationY = 0F;

    private readonly List<float> m_rotArrayX = new List<float>();
    private readonly List<float> m_rotArrayY = new List<float>();

    float m_rotAverageX = 0F;
    float m_rotAverageY = 0F;

    Quaternion m_originalRotation;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q)) StaticGameManager.ToggleGuiCapture();
        if (StaticGameManager.GuiCapturesMouse()) return;


        switch (axes)
        {
            case RotationAxes.MouseXAndY:
            {
                //Resets the average rotation
                m_rotAverageY = 0f;
                m_rotAverageX = 0f;

                //Gets rotational input from the mouse
                m_rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                m_rotationX += Input.GetAxis("Mouse X") * sensitivityX;

                //Adds the rotation values to their relative array
                m_rotArrayY.Add(m_rotationY);
                m_rotArrayX.Add(m_rotationX);

                //If the arrays length is bigger or equal to the value of frameCounter remove the first value in the array
                if (m_rotArrayY.Count >= frameCounter)
                {
                    m_rotArrayY.RemoveAt(0);
                }

                if (m_rotArrayX.Count >= frameCounter)
                {
                    m_rotArrayX.RemoveAt(0);
                }

                //Adding up all the rotational input values from each array
                foreach (var rot in m_rotArrayY)
                {
                    m_rotAverageY += rot;
                }

                foreach (var rot in m_rotArrayX)
                {
                    m_rotAverageX += rot;
                }

                //Standard maths to find the average
                m_rotAverageY /= m_rotArrayY.Count;
                m_rotAverageX /= m_rotArrayX.Count;

                //Clamp the rotation average to be within a specific value range
                m_rotAverageY = ClampAngle(m_rotAverageY, minimumY, maximumY);
                m_rotAverageX = ClampAngle(m_rotAverageX, minimumX, maximumX);

                //Get the rotation you will be at next as a Quaternion
                Quaternion yQuaternion = Quaternion.AngleAxis(m_rotAverageY, Vector3.left);
                Quaternion xQuaternion = Quaternion.AngleAxis(m_rotAverageX, Vector3.up);

                //Rotate
                transform.localRotation = m_originalRotation * xQuaternion * yQuaternion;
                break;
            }

            case RotationAxes.MouseX:
            {
                m_rotAverageX = 0f;
                m_rotationX += Input.GetAxis("Mouse X") * sensitivityX;
                m_rotArrayX.Add(m_rotationX);
                if (m_rotArrayX.Count >= frameCounter)
                {
                    m_rotArrayX.RemoveAt(0);
                }

                foreach (var rot in m_rotArrayX)
                {
                    m_rotAverageX += rot;
                }

                m_rotAverageX /= m_rotArrayX.Count;
                m_rotAverageX = ClampAngle(m_rotAverageX, minimumX, maximumX);
                Quaternion xQuaternion = Quaternion.AngleAxis(m_rotAverageX, Vector3.up);
                transform.localRotation = m_originalRotation * xQuaternion;
                break;
            }

            default:
            {
                m_rotAverageY = 0f;
                m_rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                m_rotArrayY.Add(m_rotationY);
                if (m_rotArrayY.Count >= frameCounter)
                {
                    m_rotArrayY.RemoveAt(0);
                }

                foreach (var rot in m_rotArrayY)
                {
                    m_rotAverageY += rot;
                }

                m_rotAverageY /= m_rotArrayY.Count;
                m_rotAverageY = ClampAngle(m_rotAverageY, minimumY, maximumY);
                Quaternion yQuaternion = Quaternion.AngleAxis(m_rotAverageY, Vector3.left);
                transform.localRotation = m_originalRotation * yQuaternion;
                break;
            }
        }
    }

    void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        if (rb)
            rb.freezeRotation = true;
        m_originalRotation = transform.localRotation;
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle %= 360;
        if (!(angle >= -360F) || !(angle <= 360F)) return Mathf.Clamp(angle, min, max);
        if (angle < -360F)
        {
            angle += 360F;
        }
        if (angle > 360F)
        {
            angle -= 360F;
        }

        return Mathf.Clamp(angle, min, max);
    }
}