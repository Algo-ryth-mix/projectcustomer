﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryReactor : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {

        //check if the inventory is open
        if (StaticGameManager.GuiCapturesMouse())
        {
            //enable the background
            transform.GetChild(0).gameObject.SetActive(true);

            //set the text to inform how to close the Inventory
            transform.GetChild(1).GetComponent<Text>().text = "Press Q to close Inventory";
        }
        else
        {
            //disable the background
            transform.GetChild(0).gameObject.SetActive(false);
            

            //set the text to inform how to open the Inventory
            transform.GetChild(1).GetComponent<Text>().text = "Press Q to open Inventory";
        }


    }
}
