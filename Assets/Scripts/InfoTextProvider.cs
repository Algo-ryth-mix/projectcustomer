﻿using UnityEngine;
using UnityEngine.UI;

public class InfoTextProvider : MonoBehaviour
{
    private Text m_text;

    private float timeout = 0;

    private void Awake()
    {
        //register at the game manager
        StaticGameManager.InfoText = this;
    }

    private void Start()
    { 
        m_text = GetComponent<Text>();
    }


    private void Update()
    {
        //check if the text has expired
        if (timeout <= 0)
        {
            m_text.enabled = false;
            //set it to nothing
            m_text.text = "";
        }
        else timeout -= Time.deltaTime;
    }

    public void SetInfoText(string text,bool overrule = false)
    {
        //check if the text can be set
        if (timeout < 0.05f||overrule) {
            
            //set the text
            m_text.text = text;
            //refresh the timeout
            timeout = 1;

            //enable the text
            m_text.enabled = true;
        }
    }
}
