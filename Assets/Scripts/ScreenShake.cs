﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour
{


    public Animator camAnim;

    public void StartShake()
    {
        //shake screen
        camAnim.SetBool("shake",true);
    }

    public void StopShake()
    {
        //stop shaking the screen 
        camAnim.SetBool("shake",false);
    }

}
