﻿using System;
using UnityEngine;

public class EffectBase : MonoBehaviour
{
    /**@brief implements a Runner that can be called by an Item
     * use this instead of MonoBehaviour in any effector basis
     */
    public Action Runner;
}
