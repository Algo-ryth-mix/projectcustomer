﻿using System.Threading.Tasks;
using UnityEngine;

public class PoisonEffect : EffectBase
{
    private GameObject m_child;

    //create inline async dispatcher class 
    private class AsyncPoison
    {
        //start the effect basis
        public async void Start(GameObject child)
        {
            //wait 10 * 500 ms and apply negative effect
            for (int i = 0; i < 10; i++)
            {
                ResourceTick.getResource("Thirst").addResource(-4F);
                await Task.Delay(500);
            }
           
            //disable 
            child.SetActive(false);
        }
    }

    public void Awake()
    {
        //get the "sick-border"
        m_child = transform.GetChild(0).gameObject;

        Runner = () =>
        {
            //enable it 
            m_child.SetActive(true);

            //run the poison effect
            AsyncPoison poison = new AsyncPoison();
            poison.Start(m_child);
        };
    }

}
