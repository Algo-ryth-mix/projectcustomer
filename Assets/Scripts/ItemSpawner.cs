﻿using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    //associated item that can be spawned over and over again
    public ItemContainer item;


    public void LookAt(GameObject other)
    {
        //inform the player that he can press E
        StaticGameManager.InfoText.SetInfoText("Press E to use the " + gameObject.name);

        //check if he did
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (other.CompareTag(StaticGameManager.PLAYER_TAG))
            {
                //insert the item into the players inventory
                var player = other.GetComponent<Player>();
                player.InsertInventory(item.GetItem());
            }
        }
    }
}
