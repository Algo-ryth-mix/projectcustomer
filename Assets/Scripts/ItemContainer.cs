﻿using System;
using System.Resources;
using Assets.Scripts;
using UnityEngine;

public class ItemContainer : MonoBehaviour
{
    [SerializeField] private string m_itemId = "missing_item_id";

    [Flags]
    public enum ItemActionSelector : int
    {
        Nothing = 0,
        AffectsResource = 1,
        ContainsItem = 2,
        HasRunner = 4,
        AffectsResourceAndContainsItem = AffectsResource | ContainsItem,
        AffectsResourceAndHasRunner = AffectsResource | HasRunner,
        ContainsItemAndHasRunner = ContainsItem | HasRunner,
    }

    public ItemActionSelector ItemAction = ItemActionSelector.Nothing;

    public string AffectedResource = "No Resource";
    public float AffectedResourceAmount = 1F;
    public ItemContainer ContainedItem;
    public EffectBase Effect;


    public bool HasPreRequisite = false;
    public string PreRequisiteId = "missing_item_id";


    private Item m_item;

    public Item GetItem() => m_item;

    private static AudioClip m_clip = null;

    private static AudioSource m_pickUpSoundSource;


    private void Awake()
    {

        //prepare the pickup-sound
        if(m_clip == null) m_clip = Resources.Load<AudioClip>("Sounds/pick_up");
        m_pickUpSoundSource = gameObject.AddComponent<AudioSource>();
        m_pickUpSoundSource.clip = m_clip;
        m_pickUpSoundSource.volume = 0.2F;


        //Composite the Action that happens when somebody clicks the item
        Action a = () => {};

        //The action affects a resource
        if((ItemAction & ItemActionSelector.AffectsResource) != 0)
            a += () => ResourceTick.getResource(AffectedResource)?.addResource(AffectedResourceAmount);

        //the action spawns another item
        if ((ItemAction & ItemActionSelector.ContainsItem) != 0)
            a += () => StaticGameManager.PlayerContainer.InsertInventory(ContainedItem.m_item);

        //the action has an Effector that is defined via an EffectBase
        if ((ItemAction & ItemActionSelector.HasRunner) != 0)
            a += () => Effect.Runner();

        //create the item
        m_item = StaticGameManager.ItemFromId(m_itemId);
        m_item?.SetUseEvent(a);


        //set the prerequisite
        if(HasPreRequisite)
            m_item?.SetPreRequisite(StaticGameManager.ItemFromId(PreRequisiteId));

    }
    
    //this will be called by the Player Object ... any other caller is unexpected and will probably crash the game
    public void LookAt(GameObject other)
    {
        //inform the player that he has to press E
        StaticGameManager.InfoText.SetInfoText("Press E to pick-up " + m_item);

        //check if the Button was pressed
        if (Input.GetKeyDown(KeyCode.E))
        {

            //if it was check if it really was the player that invoked us
            if (other.CompareTag(StaticGameManager.PLAYER_TAG))
            {

                var player = other.GetComponent<Player>();


                //insert the item into the inventory of the player
                if (player.InsertInventory(m_item))
                {
                    //if that succeeded play the pickup sound and die
                    m_pickUpSoundSource.Play();
                    Destroy(gameObject);
                }
            }

        }
    }


}
