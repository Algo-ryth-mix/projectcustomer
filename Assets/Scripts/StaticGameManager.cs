﻿using System;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

using SSRegistry = System.Collections.Generic.Dictionary<string,string>;

public class StaticGameManager : MonoBehaviour
{

    //public tags
    public const string PLAYER_TAG = "Player";
    public const string TERRAIN_TAG = "Terrain";

    //public accessible player-object (use after awake)
    public static Player PlayerContainer { set; get; }
    public static Tuple<GameObject, Item>[] PlayerInventory = new Tuple<GameObject, Item>[7];

    //public accessible info-text provider (use after awake)
    public static InfoTextProvider InfoText { get; set; }
    

    private static Dictionary<string,SSRegistry> m_GlobalSceneParams = new Dictionary<string, SSRegistry>
    {
        {"GameScene",new Dictionary<string,string>
        {
            {"LevelTime","10"},
        }},
        {"Bathroom",new Dictionary<string,string>
        {
            {"LevelTime","10"},
        }},
    };

    //REGISTER ITEMS HERE (id->name) 
    private static SSRegistry m_knownItems = new Dictionary<string, string>
    {
        {"missing_item_id","Invalid Item"},
        {"bottled_water_item_id","Bottled Water"},
        {"ham_item_id","Literally Ham"},
        {"radio_item_id","FM-Radio"},
        {"battery_item_id","AA-Battery"},
        {"milk_item_id","Milk"},
        {"canned_food_item_id","Canned Food"},
        {"dirty_water_item_id","Dirty Water"},
        {"medkit_item_id","Medikit" }
    };



    /**@brief get params based on the scene-name*/
    public static SSRegistry GetSceneParams()
    {
        return m_GlobalSceneParams[SceneManager.GetActiveScene().name];
    }

    /**@brief get the Item based on id*/
    public static Item ItemFromId(string itemId)
    {
        string name = "Unknown";
        if (m_knownItems.ContainsKey(itemId)) name = m_knownItems[itemId];

        return new Item(name, itemId);
    }


    private static bool m_capturesMouse;

    public static void ToggleGuiCapture()
    {
        m_capturesMouse = !m_capturesMouse;
    }

    /**@brief checks if the gui captures the mouse=player opened inventory*/
    public static bool GuiCapturesMouse()
    {
        return m_capturesMouse;
    }
}
