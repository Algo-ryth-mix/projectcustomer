﻿using UnityEngine;

public class CelestialBody : MonoBehaviour
{
    void Update()
    {

        //slowly but gently rotate the sun around the origin
        transform.RotateAround(Vector3.zero, Vector3.right,Time.deltaTime);
        transform.LookAt(Vector3.zero);
    }
}
