﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[AddComponentMenu("MovementControl/FPS Walker")]
public class MovementController : MonoBehaviour
{

    public float speed = 10.0f;
    public float gravity = 10.0f;
    public float maxVelocityChange = 10.0f;
    public bool canJump = true;
    public float jumpHeight = 2.0f;
    private bool m_grounded = false;

    public Transform AffectingTransform;

    private Rigidbody m_rigidbody;

    public AudioClip WalkingClip = null;
    public AudioSource WalkingSource = null;

    void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        if (WalkingClip != null)
        {
            WalkingSource = gameObject.AddComponent<AudioSource>();
            WalkingSource.clip = WalkingClip;
            WalkingSource.loop = true;
            WalkingSource.volume = 1.0F;
        }


        m_rigidbody.freezeRotation = true;
        m_rigidbody.useGravity = false;
    }


    private float m_elapsed = 0.0F;
    private bool sound_enabled = false;


    void Update()
    {
        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.S) ||
            Input.GetKeyUp(KeyCode.D))
            WalkingSource.Stop();

        if (m_grounded)
        {
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) ||
                Input.GetKeyDown(KeyCode.D))
                WalkingSource.Play();
        }
    }



    void FixedUpdate()
    {
        if (m_grounded)
        {
           


            // Calculate how fast we should be moving
            Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            targetVelocity = AffectingTransform.TransformDirection(targetVelocity);
            targetVelocity *= speed;


            // Apply a force that attempts to reach our target velocity
            Vector3 velocity = m_rigidbody.velocity;
            Vector3 velocityChange = (targetVelocity - velocity);
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
            velocityChange.y = 0;


            m_rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);

            // Jump
            if (canJump && Input.GetButton("Jump"))
            {
                m_rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
            }
        }

        // We apply gravity manually for more tuning control
        m_rigidbody.AddForce(new Vector3(0, -gravity * m_rigidbody.mass, 0));

        m_grounded = false;
    }

    void OnCollisionStay()
    {
        m_grounded = true;
    }

    float CalculateJumpVerticalSpeed()
    {
        // From the jump height and gravity we deduce the upwards speed 
        // for the character to reach at the apex.
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }
}