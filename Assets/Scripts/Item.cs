﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Assets.Scripts
{
    public class Item 
    {
        private readonly string m_id;
        private readonly string m_name;


        /**@brief returns the Associated Sprite
         * gets the sprite from the resource folder that has been associated with this Item
         * @return the requested Sprite
         */
        public Sprite GetAssociatedSprite()
        {
            return Resources.Load<Sprite>("Sprites/Items/" + GetID());
        }

        /**@brief gets the name of the Item*/
        public string GetName() => m_name;

        /**@brief gets the id of the Item*/
        public string GetID() => m_id;
        
       public Item(string name, string ID)
        {
            m_id = ID;
            m_name = name;
        }

#region EQUALITY
        /**@brief checks for equality with another Item*/
        public static bool operator ==([CanBeNull] Item rhs,[CanBeNull] Item lhs)
        {
            if ((object) rhs == null)
            {
                if ((object) lhs == null) return true;
                return false;
            } if ((object) lhs == null) return false;

            return rhs.GetID() == lhs.GetID();
        }
        /**@brief checks for equality with another item_id */
        public static bool Same( Item rhs, string lhs)
        {
            if ((object)rhs == null)
            {
                if ((object)lhs == null)return true;
                 return false;
            } if ((object)lhs == null) return false;

            return rhs.GetID() == lhs;
        }
        /**@group Equality inheritors
         * @{
         */
        public static bool operator !=([NotNull]Item rhs, [NotNull]Item lhs)
        {
            return !(rhs == lhs);
        }
        public static bool NotSame([NotNull] Item rhs, [NotNull] string lhs)
        {
            return !Same(rhs, lhs);
        }

        public override string ToString()
        {
            return m_name;
        }

        public override int GetHashCode()
        {
            return m_id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var item = obj as Item;
            return item != null &&
                   m_id == item.m_id;
        }
        /** @} */
#endregion

        private Action m_useEvent;
        private Item m_preRequisite;

        /**@brief sets the required Item to use this item
         * @param [in] i the required Item
         */
        public void SetPreRequisite(Item i)
        {
            m_preRequisite = i;
        }


        /**@brief sets the Action that is performed when the item is used
         * @param [in] a the Action that gets performed
         */
        public void SetUseEvent(Action a)
        {
            m_useEvent = a;
        }

        /**@brief Uses the item*/
        public bool RunUseEvent()
        {
            //check if the prerequisite was fulfilled
            if (m_preRequisite != null)
            {
                if (StaticGameManager.PlayerContainer.GetItemFromInventory(m_preRequisite.GetID()) == null)
                {
                    //inform the player that he is missing something
                    StaticGameManager.InfoText.SetInfoText("you are missing " + m_preRequisite + " to activate this!",true);
                    return false;
                }
                
            }

            //perform the action
            m_useEvent?.Invoke();
            return true;
        }
    }
}
