﻿using System.Collections.Generic;
using UnityEngine;

public class ItemTrigger : MonoBehaviour
{
    [SerializeField] private List<string> m_requiredItems = new List<string>(1);

    private bool m_wasTriggered = false;

    /**@brief
     * attach an action to this bool to check if the trigger was Activated 
     */
    public bool WasTriggered() => m_wasTriggered;


    public void LookAt(GameObject other)
    {
        //tell the Player to press E
        StaticGameManager.InfoText.SetInfoText("Press E to activate Trigger");



        if (Input.GetKeyDown(KeyCode.E))
        {
            if (other.CompareTag(StaticGameManager.PLAYER_TAG))
            {
                bool checkFailed = false;

                //check if the player has all required Items
                foreach (string requiredItem in m_requiredItems)
                {
                    //if he does not inform him
                    if (other.GetComponent<Player>().GetItemFromInventory(requiredItem) == null)
                    {
                        checkFailed = true;
                        StaticGameManager.InfoText.SetInfoText("You are missing " + StaticGameManager.ItemFromId(requiredItem), true);
                    }
                }

                //if he has all Items trigger the action
                if (!checkFailed)
                {
                    m_wasTriggered = true;
                }
            }
        }
    }
}
