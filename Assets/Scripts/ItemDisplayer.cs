﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ItemDisplayer : MonoBehaviour,IPointerClickHandler
{

    [SerializeField] private Image m_image;
    [SerializeField] private Text m_text;
    private Item m_item;

    public void Awake()
    {
        //make sure the children are started in an suspended state to prevent flickering
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(false);
    }


    public void Update()
    {

        //react to the Inventory system 
        if (StaticGameManager.GuiCapturesMouse())
        {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).gameObject.SetActive(true);

        }
        else
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(false);
        }
    }


    public void SetItem(Item item)
    {
        //load the sprite from the Registry
        m_image.overrideSprite = item.GetAssociatedSprite();

        //reflect name of item
        m_text.text = item.GetName();

        //setup the item
        m_item = item;
    }

    public void OnPointerClick(PointerEventData evtData)  
    {

        //this check is a bit weird as we need to cast from bool? to bool implicitly (bool? could be true,false or null)
        if (m_item?.RunUseEvent() != true) return;

        //consume Item from Inventory
        StaticGameManager.PlayerContainer.ConsumeItemFromInventory(m_item.GetID());
    }


}
