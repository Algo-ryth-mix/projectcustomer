﻿using System;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject m_imageDisplayPrefab;
    [SerializeField] private GameObject m_imageDisplayContainer;

    private Camera m_camera;

    private ScreenShake m_shake;

    //all the things that can kill a player must be here
    public List<Func<(bool,string)>> MurderCallbackActions = new List<Func<(bool, string)>>(0);


    public void Awake()
    {

        //get the screen-shaker
        m_shake = GameObject.FindGameObjectWithTag("ScreenShake").GetComponent<ScreenShake>();

        //register self
        StaticGameManager.PlayerContainer = this;
    }

    private const float ITEM_DISPLAY_OFFSET = 150;
    private const float DISTANCE_BETWEEN_ITEMS = 100;


    public void Start()
    {
        //get the params of the current scene (![note] [maybe_unused])
        var sceneParams =  StaticGameManager.GetSceneParams();

        //get the camera 
        m_camera = Camera.main;

        //start the screen-shake (![note] [only_demo])
        m_shake.StartShake();

    }

    private float m_elapsed = 0;

    public void FixedUpdate()
    {

        //if more than 5 seconds elapsed stop the screen-shake
        m_elapsed += Time.deltaTime;
        if(m_elapsed > 5) m_shake.StopShake();


        //check if the player has died for any reason
        foreach (var action in MurderCallbackActions)
        {
            var (result,message) = action();
            if (result)
            {
                //tell why the player has died
                Debug.Log("Player died because: " + message);

                //HACK(algorythmix) destroy the player to indicate death 
                //(![note] [only_demo])
                Destroy(gameObject);
            }
        }
    }


    public void Update()
    {

        //check if the player is looking at something
        Ray ray = m_camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit,3))
        {

            //give a visual representation in the Debug View
            Debug.DrawLine(ray.origin, hit.point, Color.black);
            if (hit.collider.CompareTag("Items"))
            {
                //item action
                hit.transform.GetComponent<ItemContainer>().LookAt(gameObject);
                Debug.DrawLine(ray.origin,hit.point,Color.red);
            }
            else if (hit.collider.CompareTag("ItemTrigger"))
            {

                //trigger action
                hit.transform.GetComponent<ItemTrigger>().LookAt(gameObject);
                Debug.DrawLine(ray.origin, hit.point, Color.red);
            }
            else if (hit.collider.CompareTag("ItemSpawner"))
            {
                //spawner action
                hit.transform.GetComponent<ItemSpawner>().LookAt(gameObject);
                Debug.DrawLine(ray.origin, hit.point, Color.red);
            }
        }

    }



    private int m_heldItems = 0;

    public bool InsertInventory(Item item)
    {

        //check if the player holds more than 5 items
        if (m_heldItems == 5) return false;
        m_heldItems++;
        int i = 0;

        //check what our offset is 
        for (; i < StaticGameManager.PlayerInventory.Length; i++)
        {
            if (StaticGameManager.PlayerInventory[i] == null) break;
        }


        //create the item container
        var itemDisplay =  Instantiate(m_imageDisplayPrefab, m_imageDisplayContainer.transform, true);

        //set up item container
        itemDisplay.GetComponent<ItemDisplayer>().SetItem(item);


        //position item container
        RectTransform tf = itemDisplay.GetComponent<RectTransform>();

        tf.SetParent(m_imageDisplayContainer.transform,false);
        tf.anchorMin = new Vector2(0,1);
        tf.anchorMax = new Vector2(0,1);
        tf.anchoredPosition = new Vector2(ITEM_DISPLAY_OFFSET + DISTANCE_BETWEEN_ITEMS * i,-250);

        //insert into inventory
        StaticGameManager.PlayerInventory[i] =  new Tuple<GameObject,Item>(itemDisplay,item);

        Debug.Log("Received Item: " +item.ToString());
        return true;
    }

    public Item GetItemFromInventory(string id)
    {

        //for all items
        foreach (var i in StaticGameManager.PlayerInventory)
        {
            if(i == null) continue;

            //check if this is the item
            if (Item.Same(i.Item2,id))
            {
                //return the item part of the inventory
                return i.Item2;
            }
        }

        return null;
    }

    //Note that this will crash in the event that a player picks up more than a 100 Items... not to worried about that
    public bool ConsumeItemFromInventory(string id)
    {
        //for all items
        for (int i = 0; i < StaticGameManager.PlayerInventory.Length; ++i)
        {
            //check if we reached the end of the inventory
            if (StaticGameManager.PlayerInventory[i] == null) return false;

            //check if this is the item we want
            if (Item.Same(StaticGameManager.PlayerInventory[i].Item2, id))
            {

                //destroy it
                Destroy(StaticGameManager.PlayerInventory[i].Item1);

                //reduce the amount of held items
                m_heldItems--;

                //if there are no other items past this
                if (StaticGameManager.PlayerInventory[i + 1] == null)
                {
                    //remove the entry of the item from the inventory
                    StaticGameManager.PlayerInventory[i] = null;
                    return true;
                }
                //otherwise for all past items
                for (int j = i + 1 ; StaticGameManager.PlayerInventory[j] != null; j++)
                {
                    //replace the prior entry with the entry from the next item
                    StaticGameManager.PlayerInventory[j - 1] = StaticGameManager.PlayerInventory[j];

                    //destroy the next entry
                    StaticGameManager.PlayerInventory[j] = null;

                    //move the current entry back a bit
                    RectTransform tf = StaticGameManager.PlayerInventory[j - 1].Item1.GetComponent<RectTransform>();
                    tf.anchoredPosition = tf.anchoredPosition - new Vector2(DISTANCE_BETWEEN_ITEMS, 0);

                }

                return true;
            }
        }
        
        return false;
    }

}
